package com.mygame.fbhelpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetLoader {

    public static Animation<TextureRegion> birdAnimation;
    public static BitmapFont font, shadow, whiteFont, geosansFont;
    private static Preferences preferences;
    public static Sound soundDead, soundFlap, soundCoin, soundFall;
    public static Texture texture, logoTexture;
    public static TextureRegion logo, fbLogo, backGround, grass, bird, birdDown,
            birdUp, endBarUp, endBarDown, bar, playButtonUp, playButtonDown,
            ready, gameOver, highScore, scoreboard, coin, noCoin, retryButtonUp, retryButtonDown, tap,
            skinsButtonUp, skinsButtonDown, skinsText, aboutButtonUp, aboutButtonDown, about;

    public static void load() {

        logoTexture = new Texture(Gdx.files.internal("data/logo.png"));
        logoTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        logo = new TextureRegion(logoTexture, 0, 0, 495, 128);

        texture = new Texture(Gdx.files.internal("data/texture.png"));
        texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        ready = new TextureRegion(texture, 104, 104, 87, 22);
        ready.flip(false, true);

        about = new TextureRegion(texture, 41, 81, 53, 19);
        about.flip(false, true);

        tap = new TextureRegion(texture, 172, 18, 41, 51);
        tap.flip(false, true);

        gameOver = new TextureRegion(texture, 104, 82, 94, 19);
        gameOver.flip(false, true);

        skinsText = new TextureRegion(texture, 50, 80, 48, 19);
        skinsText.flip(false, true);

        fbLogo = new TextureRegion(texture, 4, 104, 96, 22);
        fbLogo.flip(false, true);

        scoreboard = new TextureRegion(texture, 258, 2, 113, 58);
        scoreboard.flip(false, true);

        coin = new TextureRegion(texture, 3, 56, 22, 22);
        coin.flip(false, true);

        noCoin = new TextureRegion(texture, 3, 79, 22, 22);
        noCoin.flip(false, true);

        playButtonUp = new TextureRegion(texture, 216, 15, 40, 14);
        playButtonUp.flip(false, true);

        playButtonDown = new TextureRegion(texture, 216, 31, 40, 14);
        playButtonDown.flip(false, true);

        retryButtonUp = new TextureRegion(texture, 216, 0, 40, 14);
        retryButtonUp.flip(false, true);

        retryButtonDown = new TextureRegion(texture, 216, 46, 40, 14);
        retryButtonDown.flip(false, true);

        skinsButtonUp = new TextureRegion(texture, 216, 62, 40, 14);
        skinsButtonUp.flip(false, true);

        skinsButtonDown = new TextureRegion(texture, 258, 62, 40, 14);
        skinsButtonDown.flip(false, true);

        aboutButtonUp = new TextureRegion(texture, 216, 78, 40, 14);
        aboutButtonUp.flip(false, true);

        aboutButtonDown = new TextureRegion(texture, 258, 78, 40, 14);
        aboutButtonDown.flip(false, true);

        highScore = new TextureRegion(texture, 193, 104, 94, 19);
        highScore.flip(false, true);

        backGround = new TextureRegion(texture, 0, 0, 136, 43);
        backGround.flip(false, true);

        grass = new TextureRegion(texture, 0, 43, 143, 11);
        grass.flip(false, true);

        birdDown = new TextureRegion(texture, 136, 0, 17, 12);
        birdDown.flip(false, true);

        bird = new TextureRegion(texture, 153, 0, 17, 12);
        bird.flip(false, true);

        birdUp = new TextureRegion(texture, 170, 0, 17, 12);
        birdUp.flip(false, true);

        TextureRegion[] birds = {birdDown, bird, birdUp};
        birdAnimation = new Animation<TextureRegion>(15.0E-02f, birds);
        birdAnimation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

        endBarUp = new TextureRegion(texture, 192, 0, 24, 14);
        endBarDown = new TextureRegion(endBarUp);
        endBarDown.flip(false, true);

        bar = new TextureRegion(texture, 136, 16, 22, 3);
        bar.flip(false, true);

        soundDead = Gdx.audio.newSound(Gdx.files.internal("data/dead.wav"));
        soundFlap = Gdx.audio.newSound(Gdx.files.internal("data/flap.wav"));
        soundCoin = Gdx.audio.newSound(Gdx.files.internal("data/coin.wav"));
        soundFall = Gdx.audio.newSound(Gdx.files.internal("data/fall.wav"));

        font = new BitmapFont(Gdx.files.internal("data/text.fnt"));
        font.getData().setScale(.25f, -.25f);

        geosansFont = new BitmapFont(Gdx.files.internal("data/geosans-light32.fnt"));
        geosansFont.setColor(Color.BLACK);
        geosansFont.getData().setScale(.18f, -.15f);

        whiteFont = new BitmapFont(Gdx.files.internal("data/whitetext.fnt"));
        whiteFont.getData().setScale(.1f, -.1f);

        shadow = new BitmapFont(Gdx.files.internal("data/shadow.fnt"));
        shadow.getData().setScale(.25f, -.25f);

        preferences = Gdx.app.getPreferences("FlappyBird");
        if (!preferences.contains("highScore")) {
            preferences.putInteger("highScore", 0);
        }
    }

    public static void setHighScore(int val) {
        preferences.putInteger("highScore", val);
        preferences.flush();
    }

    public static int getHighScore() {
        return preferences.getInteger("highScore");
    }

    public static void dispose() {
        texture.dispose();
        soundDead.dispose();
        soundFlap.dispose();
        soundCoin.dispose();
        font.dispose();
        geosansFont.dispose();
        shadow.dispose();
    }
}
