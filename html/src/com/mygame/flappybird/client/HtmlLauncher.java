package com.mygame.flappybird.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.mygame.flappybird.FBGame;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig() {
                return new GwtApplicationConfiguration(272, 408);
        }

        @Override
        public ApplicationListener createApplicationListener() {
                return new FBGame();
        }
}