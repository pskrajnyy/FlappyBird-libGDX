package com.mygame.flappybird;

import com.badlogic.gdx.Game;
import com.mygame.fbhelpers.AssetLoader;
import com.mygame.screens.SplashScreen;

public class FBGame extends Game {
    @Override
    public void create() {
        AssetLoader.load();
        setScreen(new SplashScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }
}
