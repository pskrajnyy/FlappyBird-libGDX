package com.mygame.gameobject;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

public class Pipe extends Scrollable {
    private Random random;
    private Rectangle endBarUp, endBarDown, barUp, barDown;
    public static final int VERTICAL_GAP = 55;
    public static final int END_BAR_WIDTH = 24;
    public static final int END_BAR_HEIGHT = 11;
    private float groundY;
    private boolean isScored = false;

    public Pipe(float x, float y, int width, int height, float scrollSpeed, float groundY) {
        super(x, y, width, height, scrollSpeed);
        this.groundY = groundY;
        random = new Random();
        endBarUp = new Rectangle();
        endBarDown = new Rectangle();
        barUp = new Rectangle();
        barDown = new Rectangle();
    }

    @Override
    public void update(float delta) {
        super.update(delta);

        barUp.set(position.x, position.y, width, height);
        barDown.set(position.x, position.y + height + VERTICAL_GAP, width, groundY - (position.y + height + VERTICAL_GAP));

        endBarUp.set(position.x - (END_BAR_WIDTH - width) / 2, position.y + height - END_BAR_HEIGHT, END_BAR_WIDTH, END_BAR_HEIGHT);
        endBarDown.set(position.x - (END_BAR_WIDTH - width) / 2, barDown.y, END_BAR_WIDTH, END_BAR_HEIGHT);
    }

    @Override
    public void reset(float newX) {
        super.reset(newX);
        height = random.nextInt(90) + 15;
        isScored = false;
    }

    public void onRestart(float x, float scrollSpeed) {
        velocity.x = scrollSpeed;
        reset(x);
    }

    public boolean collides(Bird bird) {
        if (position.x < bird.getX() + bird.getWidth()) {
            return (Intersector.overlaps(bird.getBoundingCircle(), barUp)
                    || Intersector.overlaps(bird.getBoundingCircle(), barDown)
                    || Intersector.overlaps(bird.getBoundingCircle(), endBarUp)
                    || Intersector.overlaps(bird.getBoundingCircle(), endBarDown));
        }
        return false;
    }

    public boolean isScored() {
        return isScored;
    }

    public void setScored(boolean b) {
        isScored = b;
    }
}
