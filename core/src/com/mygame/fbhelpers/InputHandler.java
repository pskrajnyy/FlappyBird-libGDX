package com.mygame.fbhelpers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.mygame.gameobject.Bird;
import com.mygame.gameworld.GameWorld;
import com.mygame.ui.SimpleButton;

import java.util.ArrayList;
import java.util.List;

public class InputHandler implements InputProcessor {

    private Bird bird;
    private GameWorld gameWorld;
    private List<SimpleButton> menuButtons;
    private SimpleButton playButton, retryButton, skinsButton, aboutButton;
    private float scaleFactorX;
    private float scaleFactorY;

    public InputHandler(GameWorld gameWorld, float scaleFactorX, float scaleFactorY) {
        this.gameWorld = gameWorld;
        this.scaleFactorX = scaleFactorX;
        this.scaleFactorY = scaleFactorY;
        bird = gameWorld.getBird();
        int midPointY = gameWorld.getMidPointY();
        menuButtons = new ArrayList<SimpleButton>();
        playButton = new SimpleButton(
                136 / 2 - (AssetLoader.playButtonUp.getRegionWidth() / 2),
                midPointY + 20, 40, 14, AssetLoader.playButtonUp,
                AssetLoader.playButtonDown);
        retryButton = new SimpleButton(
                136 / 2 - (AssetLoader.retryButtonUp.getRegionWidth() / 2),
                midPointY + 40, 40, 14, AssetLoader.retryButtonUp,
                AssetLoader.retryButtonDown);
        aboutButton = new SimpleButton(
                136 / 2 - (AssetLoader.aboutButtonUp.getRegionWidth() / 2),
                midPointY + 40, 40, 14, AssetLoader.aboutButtonUp,
                AssetLoader.aboutButtonDown);
        menuButtons.add(playButton);
        menuButtons.add(retryButton);
        menuButtons.add(aboutButton);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        screenX = scaleX(screenX);
        screenY = scaleY(screenY);

        if (gameWorld.isMenu()) {
            playButton.isTouchDown(screenX, screenY);
            aboutButton.isTouchDown(screenX, screenY);
        } else if (gameWorld.isReady()) {
            gameWorld.start();
        }

        bird.onClick();

        if (gameWorld.isGameOver() || gameWorld.isHighScore()) {
            retryButton.isTouchDown(screenX, screenY);
        }

        if(gameWorld.isAbout()) {
            retryButton.isTouchDown(screenX, screenY);
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        screenX = scaleX(screenX);
        screenY = scaleY(screenY);

        if (gameWorld.isMenu()) {
            if (playButton.isTouchUp(screenX, screenY)) {
                gameWorld.ready();
                return true;
            } else if (aboutButton.isTouchUp(screenX, screenY)) {
                gameWorld.about();
                return true;
            }
        }

        if (gameWorld.isGameOver() || gameWorld.isHighScore()) {
            if (retryButton.isTouchUp(screenX, screenY)) {
                gameWorld.restart();
                return true;
            }
        } else if (gameWorld.isAbout()) {
            if(retryButton.isTouchUp(screenX, screenY)) {
                gameWorld.menu();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.SPACE) {

            if (gameWorld.isMenu()) {
                gameWorld.ready();
            } else if (gameWorld.isReady()) {
                gameWorld.start();
            }

            bird.onClick();

            if (gameWorld.isGameOver() || gameWorld.isHighScore()) {
                gameWorld.restart();
            }
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    private int scaleX(int screenX) {
        return (int) (screenX / scaleFactorX);
    }

    private int scaleY(int screenY) {
        return (int) (screenY / scaleFactorY);
    }

    public List<SimpleButton> getMenuButtons() {
        return menuButtons;
    }
}
