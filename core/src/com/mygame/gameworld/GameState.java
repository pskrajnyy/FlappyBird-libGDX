package com.mygame.gameworld;

public enum GameState {
   MENU,
   READY,
   ABOUT,
   RUNNING,
   GAMEOVER,
   HIGHSCORE
}
