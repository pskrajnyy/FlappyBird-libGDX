package com.mygame.gameworld;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygame.TweenAccessors.Value;
import com.mygame.TweenAccessors.ValueAccessor;
import com.mygame.fbhelpers.AssetLoader;
import com.mygame.fbhelpers.InputHandler;
import com.mygame.gameobject.Bird;
import com.mygame.gameobject.Grass;
import com.mygame.gameobject.Pipe;
import com.mygame.gameobject.ScrollHandler;
import com.mygame.ui.SimpleButton;

import java.util.List;


public class GameRenderer {

    private GameWorld gameWorld;
    private OrthographicCamera orthographicCamera;
    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private int midPointY;
    private Bird bird;
    private ScrollHandler scrollHandler;
    private Grass frontGrass, backGrass;
    private Pipe pipe1, pipe2, pipe3;
    private TextureRegion bg, grass, birdMid, endBarUp, endBarDown, bar, ready, about,
            fbLogo, gameOver, skinsText, highScore, scoreboard, coin, noCoin, tap;
    private Animation birdAnimation;
    private TweenManager manager;
    private Value alpha = new Value();
    private List<SimpleButton> menuButtons;
    private Color transitionColor;

    public GameRenderer(GameWorld gameWorld, int gameHeight, int midPointY) {
        this.gameWorld = gameWorld;
        this.midPointY = midPointY;
        this.menuButtons = ((InputHandler) Gdx.input.getInputProcessor()).getMenuButtons();
        orthographicCamera = new OrthographicCamera();
        orthographicCamera.setToOrtho(true, 136, gameHeight);
        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(orthographicCamera.combined);
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(orthographicCamera.combined);
        initGameObjects();
        initAssets();
        transitionColor = new Color();
        prepareTransition(255, 255, 255, .5f);
    }

    private void initGameObjects() {
        bird = gameWorld.getBird();
        scrollHandler = gameWorld.getScrollHandler();
        frontGrass = scrollHandler.getFrontGrass();
        backGrass = scrollHandler.getBackGrass();
        pipe1 = scrollHandler.getPipe1();
        pipe2 = scrollHandler.getPipe2();
        pipe3 = scrollHandler.getPipe3();
    }

    private void initAssets() {
        bg = AssetLoader.backGround;
        grass = AssetLoader.grass;
        birdAnimation = AssetLoader.birdAnimation;
        birdMid = AssetLoader.bird;
        endBarUp = AssetLoader.endBarUp;
        endBarDown = AssetLoader.endBarDown;
        bar = AssetLoader.bar;
        ready = AssetLoader.ready;
        fbLogo = AssetLoader.fbLogo;
        gameOver = AssetLoader.gameOver;
        highScore = AssetLoader.highScore;
        scoreboard = AssetLoader.scoreboard;
        coin = AssetLoader.coin;
        noCoin = AssetLoader.noCoin;
        tap = AssetLoader.tap;
        skinsText = AssetLoader.skinsText;
        about = AssetLoader.about;
    }

    private void drawGrass() {
        spriteBatch.draw(grass, frontGrass.getX(), frontGrass.getY(), frontGrass.getWidth(), frontGrass.getHeight());
        spriteBatch.draw(grass, backGrass.getX(), backGrass.getY(), backGrass.getWidth(), backGrass.getHeight());
    }

    private void drawEndsBars() {
        spriteBatch.draw(endBarUp, pipe1.getX() - 1, pipe1.getY() + pipe1.getHeight() - 14, 24, 14);
        spriteBatch.draw(endBarDown, pipe1.getX() - 1, pipe1.getY() + pipe1.getHeight() + 45, 24, 14);

        spriteBatch.draw(endBarUp, pipe2.getX() - 1, pipe2.getY() + pipe2.getHeight() - 14, 24, 14);
        spriteBatch.draw(endBarDown, pipe2.getX() - 1, pipe2.getY() + pipe2.getHeight() + 45, 24, 14);

        spriteBatch.draw(endBarUp, pipe3.getX() - 1, pipe3.getY() + pipe3.getHeight() - 14, 24, 14);
        spriteBatch.draw(endBarDown, pipe3.getX() - 1, pipe3.getY() + pipe3.getHeight() + 45, 24, 14);
    }

    private void drawPipes() {
        spriteBatch.draw(bar, pipe1.getX(), pipe1.getY(), pipe1.getWidth(), pipe1.getHeight());
        spriteBatch.draw(bar, pipe1.getX(), pipe1.getY() + pipe1.getHeight() + 45, pipe1.getWidth(), midPointY + 66 - (pipe1.getHeight() + 45));

        spriteBatch.draw(bar, pipe2.getX(), pipe2.getY(), pipe2.getWidth(), pipe2.getHeight());
        spriteBatch.draw(bar, pipe2.getX(), pipe2.getY() + pipe2.getHeight() + 45, pipe2.getWidth(), midPointY + 66 - (pipe2.getHeight() + 45));

        spriteBatch.draw(bar, pipe3.getX(), pipe3.getY(), pipe3.getWidth(), pipe3.getHeight());
        spriteBatch.draw(bar, pipe3.getX(), pipe3.getY() + pipe3.getHeight() + 45, pipe3.getWidth(), midPointY + 66 - (pipe3.getHeight() + 45));
    }

    private void drawBirdCentered(float runTime) {
        spriteBatch.draw((TextureRegion) birdAnimation.getKeyFrame(runTime), 59, bird.getY() - 15,
                bird.getWidth() / 2.0f, bird.getHeight() / 2.0f,
                bird.getWidth(), bird.getHeight(), 1, 1, bird.getRotation());
    }

    private void drawBird(float runTime) {

        if (bird.shouldntFlap()) {
            spriteBatch.draw(birdMid, bird.getX(), bird.getY(),
                    bird.getWidth() / 2.0f, bird.getHeight() / 2.0f,
                    bird.getWidth(), bird.getHeight(), 1, 1, bird.getRotation());
        } else {
            spriteBatch.draw((TextureRegion) birdAnimation.getKeyFrame(runTime), bird.getX(),
                    bird.getY(), bird.getWidth() / 2.0f,
                    bird.getHeight() / 2.0f, bird.getWidth(), bird.getHeight(),
                    1, 1, bird.getRotation());
        }
    }

    private void drawScoreboard() {
        spriteBatch.draw(scoreboard, 10, midPointY - 30, 113, 58);
        spriteBatch.draw(noCoin, 15, midPointY - 10, 22, 22);
        spriteBatch.draw(noCoin, 38, midPointY - 10, 22, 22);
        spriteBatch.draw(noCoin, 61, midPointY - 10, 22, 22);

        if (gameWorld.getScore() > 20) {
            spriteBatch.draw(coin, 61, midPointY - 10, 22, 22);
        }
        if (gameWorld.getScore() > 50) {
            spriteBatch.draw(coin, 38, midPointY - 10, 22, 22);
        }
        if (gameWorld.getScore() > 100) {
            spriteBatch.draw(coin, 15, midPointY - 10, 22, 22);
        }
        int length = ("" + gameWorld.getScore()).length();
        AssetLoader.whiteFont.draw(spriteBatch, "" + gameWorld.getScore(), 101 - (2 * length), midPointY - 13);

        int length2 = ("" + AssetLoader.getHighScore()).length();
        AssetLoader.whiteFont.draw(spriteBatch, "" + AssetLoader.getHighScore(), 102 - (2.5f * length2), midPointY + 8);
    }

    private void drawAbout() {
        String author = "Author: Przemyslaw Skrajny";
        int length = author.length();
        String contact = "Email: pskrajnyy@gmail.com";
        int length2 = contact.length();
        spriteBatch.draw(about, 40, midPointY - 70, 53, 19);
        AssetLoader.geosansFont.draw(spriteBatch, author, 70 - (2.5f * length), midPointY - 40);
        AssetLoader.geosansFont.draw(spriteBatch, contact, 70 - (2.5f * length), midPointY + 5);
    }

    private void drawScore() {
        int length = ("" + gameWorld.getScore()).length();
        AssetLoader.shadow.draw(spriteBatch, "" + gameWorld.getScore(), 68 - (3 * length), midPointY - 82);
        AssetLoader.font.draw(spriteBatch, "" + gameWorld.getScore(), 68 - (3 * length), midPointY - 83);
    }

    private void drawHighScore() {
        spriteBatch.draw(highScore, 20, midPointY - 60, 94, 19);
    }

    private void drawMenuUI() {
        spriteBatch.draw(AssetLoader.fbLogo, 136 / 2 - 50, midPointY - 60,
                AssetLoader.fbLogo.getRegionWidth(),
                AssetLoader.fbLogo.getRegionHeight());

        menuButtons.get(0).draw(spriteBatch);
        menuButtons.get(2).draw(spriteBatch);
    }


    private void drawRetry() {
        menuButtons.get(1).draw(spriteBatch);
    }

    private void drawReady() {
        spriteBatch.draw(ready, 25, midPointY - 70, 87, 22);
        spriteBatch.draw(tap, 75, midPointY - 25);
    }

    private void drawGameOver() {
        spriteBatch.draw(gameOver, 20, midPointY - 60, 94, 19);
    }

    public void render(float delta, float runTime) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        // Draw Background color
        shapeRenderer.setColor(78 / 255.0f, 192 / 255.0f, 202 / 255.0f, 1);
        shapeRenderer.rect(0, 0, 136, midPointY + 66);

        // Draw Grass
        shapeRenderer.setColor(111 / 255.0f, 186 / 255.0f, 45 / 255.0f, 1);
        shapeRenderer.rect(0, midPointY + 66, 136, 11);

        // Draw Dirt
        shapeRenderer.setColor(222 / 255.0f, 216 / 255.0f, 149 / 255.0f, 1);
        shapeRenderer.rect(0, midPointY + 77, 136, 52);
        shapeRenderer.end();

        spriteBatch.begin();
        spriteBatch.disableBlending();
        spriteBatch.draw(bg, 0, midPointY + 23, 136, 43);

        drawPipes();
        spriteBatch.enableBlending();
        drawEndsBars();

        if (gameWorld.isRunning()) {
            drawBird(runTime);
            drawScore();
        } else if (gameWorld.isReady()) {
            drawBird(runTime);
            drawReady();
        } else if (gameWorld.isMenu()) {
            drawBirdCentered(runTime);
            drawMenuUI();
        } else if (gameWorld.isAbout()) {
            drawAbout();
            drawBirdCentered(runTime);
            drawRetry();
        } else if (gameWorld.isGameOver()) {
            drawScoreboard();
            drawBird(runTime);
            drawGameOver();
            drawRetry();
        } else if (gameWorld.isHighScore()) {
            drawScoreboard();
            drawBird(runTime);
            drawHighScore();
            drawRetry();
        }
        drawGrass();
        spriteBatch.end();
        drawTransition(delta);
    }

    public void prepareTransition(int r, int g, int b, float duration) {
        transitionColor.set(r / 255.0f, g / 255.0f, b / 255.0f, 1);
        alpha.setValue(1);
        Tween.registerAccessor(Value.class, new ValueAccessor());
        manager = new TweenManager();
        Tween.to(alpha, -1, duration).target(0).ease(TweenEquations.easeOutQuad).start(manager);
    }

    private void drawTransition(float delta) {
        if (alpha.getValue() > 0) {
            manager.update(delta);
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(transitionColor.r, transitionColor.g, transitionColor.b, alpha.getValue());
            shapeRenderer.rect(0, 0, 136, 300);
            shapeRenderer.end();
            Gdx.gl.glDisable(GL20.GL_BLEND);
        }
    }
}
