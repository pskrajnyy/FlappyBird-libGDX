# Project info
A copy of the Flappy Bird game. Game implemented in Java using [libGDX](https://libgdx.badlogicgames.com/index.html) library. 
Flappy Bird is so-called "Side-scroller" in which the player plays the role of a flying bird. The main purpose is to bypass pipes protruding from the bottom and top of the screen.

# Project objectives
- Learn basics of libGdx.
- Learn how to write simple 2D. 
- Learn how to print objects textures on the screen and how to manage these resources in Java code.

**Controls:**
- Mouse 
- Space

# Project insight
![App](/misc/SplashScreen.jpg)
![App](/misc/MainScreen.jpg)
![App](/misc/AboutScreen.JPG)
![App](/misc/ReadyScreen.jpg)
![App](/misc/GameScreen.jpg)
![App](/misc/EndScreen.jpg)

# HTML version
You can test it by yourself on this [website](https://przemyslawskrajny.itch.io/flappybird).

# Android version
You can download apk installer on this [website](https://github.com/przskraj/FlappyBird-libGDX/blob/master/android-debug.apk)
