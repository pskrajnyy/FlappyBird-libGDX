package com.mygame.gameworld;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.mygame.fbhelpers.AssetLoader;
import com.mygame.gameobject.Bird;
import com.mygame.gameobject.ScrollHandler;

public class GameWorld {
    private Bird bird;
    private ScrollHandler scrollHandler;
    private Rectangle ground;
    private int score = 0;
    private int midPointY;
    private float runTime = 0;
    private GameRenderer gameRenderer;
    private GameState currentState;

    public GameWorld(int midPointY) {
        this.midPointY = midPointY;
        currentState = GameState.MENU;
        bird = new Bird(33, midPointY - 5, 17, 12 );
        scrollHandler = new ScrollHandler(this, midPointY + 66);
        ground = new Rectangle(0, midPointY + 66, 137, 11);
    }

    public void update(float delta) {
        runTime += delta;

        switch (currentState) {
            case READY:
            case ABOUT:
            case MENU:
                updateReady(delta);
                break;
            case RUNNING:
                updateRunning(delta);
                break;
            default:
                break;
        }
    }

    public void updateReady(float delta) {
        bird.updateReady(runTime);
        scrollHandler.updateReady(delta);
    }

    public void updateRunning(float delta) {
        if (delta > .15f) {
            delta = .15f;
        }

        bird.update(delta);
        scrollHandler.update(delta);

        if (scrollHandler.collides(bird) && bird.isAlive()) {
            scrollHandler.stop();
            bird.die();
            AssetLoader.soundDead.play();
            gameRenderer.prepareTransition(255, 255, 255, .3f);
            AssetLoader.soundFall.play();
        }

        if (Intersector.overlaps(bird.getBoundingCircle(), ground)) {

            if (bird.isAlive()) {
                AssetLoader.soundDead.play();
                gameRenderer.prepareTransition(255, 255, 255, .3f);
                bird.die();
            }

            scrollHandler.stop();
            bird.decelerate();
            currentState = GameState.GAMEOVER;

            if (score > AssetLoader.getHighScore()) {
                AssetLoader.setHighScore(score);
                currentState = GameState.HIGHSCORE;
            }
        }
    }

    public void addScore(int increment) {
        score += increment;
    }

    public void start() {
        currentState = GameState.RUNNING;
    }

    public void ready() {
        currentState = GameState.READY;
        gameRenderer.prepareTransition(0, 0, 0, 1f);
    }

    public void about() {
        currentState = GameState.ABOUT;
        gameRenderer.prepareTransition(0, 0, 0, 1f);
    }

    public void menu() {
        currentState = GameState.MENU;
        gameRenderer.prepareTransition(0, 0, 0, 1f);
    }

    public void restart() {
        score = 0;
        bird.onRestart(midPointY - 5);
        scrollHandler.onRestart();
        ready();
    }

    public Bird getBird() {
        return bird;
    }

    public int getMidPointY() {
        return midPointY;
    }

    public ScrollHandler getScrollHandler() {
        return scrollHandler;
    }

    public int getScore() {
        return score;
    }

    public boolean isReady() {
        return currentState == GameState.READY;
    }

    public boolean isGameOver() {
        return currentState == GameState.GAMEOVER;
    }

    public boolean isHighScore() {
        return currentState == GameState.HIGHSCORE;
    }

    public boolean isMenu() {
        return currentState == GameState.MENU;
    }

    public boolean isAbout() { return currentState == GameState.ABOUT; }

    public boolean isRunning() {
        return currentState == GameState.RUNNING;
    }

    public void setGameRenderer(GameRenderer gameRenderer) {
        this.gameRenderer = gameRenderer;
    }
}
